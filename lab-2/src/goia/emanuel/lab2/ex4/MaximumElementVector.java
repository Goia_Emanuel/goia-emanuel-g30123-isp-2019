package goia.emanuel.lab2.ex4;

import java.util.Collections;
import java.util.Vector;


public class MaximumElementVector {


    public static void main(String[] args) {

        Vector<Double> v = new Vector<Double>();

        v.add(new Double("4.8"));
        v.add(new Double("6.4"));
        v.add(new Double("3.3"));
        v.add(new Double("2.1"));
        v.add(new Double("7.3"));

        Object obj = Collections.max(v);
        System.out.println(obj);
    }


}