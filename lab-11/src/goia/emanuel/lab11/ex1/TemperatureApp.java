package goia.emanuel.lab11.ex1;


import java.awt.BorderLayout;
import javax.swing.*;

public class TemperatureApp extends JFrame{

    TemperatureApp(Display tview){
        setLayout(new BorderLayout());
        add(tview,BorderLayout.NORTH);
        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        Sensor t = new Sensor();
        t.start();
        Display tview = new Display();
        TemperatureController tcontroler = new TemperatureController(t,tview);
        new TemperatureApp(tview);
    }
}