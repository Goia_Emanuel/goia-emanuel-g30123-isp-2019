package goia.emanuel.lab11.ex1;
import java.util.Random;
import java.util.Observable;
public class Sensor extends Observable implements Runnable {

    double temp;
    Thread t;

    public void start(){
        if(t==null){
            t = new Thread(this);
            t.start();
        }
    }

    public void run(){
        while(true){

            temp= Math.random()*100;
            this.setChanged();
            this.notifyObservers();

            try {Thread.sleep(1000);} catch (InterruptedException e) {}
        }
    }

    public double getTemperature(){
        return temp;
    }
}
