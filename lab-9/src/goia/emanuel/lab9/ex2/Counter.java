package goia.emanuel.lab9.ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Counter implements ActionListener {
    private JFrame frame;
    private JPanel panel = new JPanel ();
    private JButton buton1;
    private JTextField text;
    private int counter = 0;

    Counter(){
        frame = new JFrame ("CheckBox");
        frame.setSize (200,200);
        frame.setLocation (400,400);
        frame.add (panel);
        frame.setDefaultCloseOperation (WindowConstants.EXIT_ON_CLOSE);
        panel.setLayout (null);
        init();
        frame.setVisible (true);

    }
    public void init(){
        buton1 = new JButton ("Push!");
        buton1.setBounds (30,30,125,25);
        panel.add (buton1);
        buton1.addActionListener (this);
        text = new JTextField ();
        text.setBounds (30,75,125 ,25);
        text.setEditable (false);
        panel.add (text);
        text.addActionListener (this);

    }

    public void actionPerformed(ActionEvent e){
        if(e.getSource ()==buton1){
            counter++;
            text.setText (String.valueOf (counter));
        }
    }



}