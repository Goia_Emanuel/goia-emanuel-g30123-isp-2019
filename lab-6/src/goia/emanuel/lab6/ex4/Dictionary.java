package goia.emanuel.lab6.ex4;

import java.util.HashMap;
import java.util.Scanner;

public class Dictionary {
    private HashMap<Word,Definition> dictionary = new HashMap<>();
    private boolean valid;

    public void addWord( Word w, Definition d) {
        dictionary.put(w, d);
    }
    public Definition getDefinition(Word w){
        return dictionary.get(w);
    }

    public void getAllWords(){
        for (Word word : dictionary.keySet())
            System.out.println(word.getName());
    }

    public void getAllDefinitions() {
        for (Definition def : dictionary.values())
            System.out.println(def.getDescription());
    }

    public void readWordDefinition() {
        String cuvant;
        String descriere;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Cuvantul pe care doriti sa-l adaugati este:");
        cuvant = scanner.nextLine();
        System.out.printf("User input was: %s%n", cuvant);
        System.out.println("Descrieti cuvantul pe care l-ati adaugat:");
        descriere = scanner.next();
        Word word = new Word();
        word.setName(cuvant);
        Definition definition = new Definition();
        definition.setDescription(descriere);
        addWord(word, definition);
    }

    public void readW() {
        valid = false;
        String cuvant;
        System.out.println("Cuvantul a carei definitie o doriti:");
        Scanner scanner = new Scanner(System.in);
        cuvant = scanner.nextLine();
        Word w = new Word();
        w.setName(cuvant);
        for (Word word2 : dictionary.keySet())
            if (word2.getName().equals(w.getName())) {
                System.out.println("Definitia cuvantului cautat este: " + dictionary.get(word2).getDescription());
                valid = true;
            }
        if (!valid) System.out.println("Cuvantul introdus nu exista!");
    }

}