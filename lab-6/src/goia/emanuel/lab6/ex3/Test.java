package goia.emanuel.lab6.ex3;



public class Test {
    public static void main(String[] args) {
        BankTreeSet newBank = new BankTreeSet();

        newBank.addAcount("A", 123);
        newBank.addAcount("B", -233);
        newBank.addAcount("C", 125.5);
        newBank.addAcount("D", 88);

        newBank.printAccounts();
        System.out.println(" ");
        newBank.getAllAcounts();

        newBank.printAccounts(89,300);
    }
}
