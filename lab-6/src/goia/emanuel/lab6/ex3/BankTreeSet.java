package goia.emanuel.lab6.ex3;

import goia.emanuel.lab6.ex1.BankAccount;

import java.util.Comparator;
import java.util.TreeSet;

public class BankTreeSet {
    private TreeSet<BankAccount> tsByOwner = new TreeSet<>(Comparator.comparing(BankAccount::getOwner));
    private TreeSet<BankAccount> tsByBalance = new TreeSet<>(Comparator.comparingDouble(BankAccount::getBalance));

    public void addAcount(String owner, double balance) {
        BankAccount newAccount = new BankAccount(owner, balance);
        tsByOwner.add(newAccount);
        tsByBalance.add(newAccount);
    }

    public void printAccounts() {
        for (BankAccount i : tsByBalance)
            System.out.println(i.getOwner() + "  " + i.getBalance());
    }

    public void printAccounts(double minRange, double maxRange) {
        for (BankAccount i : tsByBalance)
            if (i.getBalance() >= minRange && i.getBalance() <= maxRange)
                System.out.println(i.getOwner() + "  " + i.getBalance());
    }

    public void getAllAcounts() {
        for (BankAccount i : tsByOwner)
            System.out.println(i.getOwner() + "  " + i.getBalance());
    }
}

