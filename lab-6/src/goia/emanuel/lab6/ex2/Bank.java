package goia.emanuel.lab6.ex2;

import goia.emanuel.lab6.ex1.BankAccount;

import java.util.ArrayList;
import java.util.Comparator;

public class Bank {
    ArrayList<BankAccount> list = new ArrayList<>();

    public void addAccount(String owner, double balance) {
        BankAccount newAccount = new BankAccount(owner, balance);
        list.add(newAccount);
    }

    public void printAccounts() {
        list.sort(Comparator.comparingDouble(BankAccount::getBalance));

        for (BankAccount i : list)
            System.out.println(i.getOwner() + " " + i.getBalance());
    }

    public void printAccounts(double minBalance, double maxBalance) {
        for (BankAccount i : list)
            if (i.getBalance() >= minBalance && i.getBalance() <= maxBalance)
                System.out.println(i.getOwner() + " " + i.getBalance());
    }

    public ArrayList<BankAccount> getAllAccounts() {
        list.sort(Comparator.comparing(BankAccount::getOwner));
        return list;
    }
}
