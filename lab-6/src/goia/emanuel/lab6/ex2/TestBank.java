package goia.emanuel.lab6.ex2;

public class TestBank {
    public static void main(String[] args) {
        Bank bank = new Bank();

        bank.addAccount("Manu", 34.5);
        bank.addAccount("Adi", 25.45);
        bank.addAccount("Cristi", -23.7);
        bank.addAccount("Alex", 124);

        bank.printAccounts();
        System.out.println("");
        System.out.println(bank.getAllAccounts());
    }
}
