package goia.emanuel.lab6.ex1;

import java.util.Objects;

public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw(double amount) {
        this.balance = this.balance - amount;
    }

    public void deposit(double amount) {
        this.balance += amount;
    }

    public String getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    @Override
    public boolean equals(Object O) {
        if (this == O) {
            return true;
        }
        if (!(O instanceof BankAccount)) {
            return false;
        }
        BankAccount B = (BankAccount) O;
        return owner.equals(B.owner) && Double.compare(balance, B.balance) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }
}
