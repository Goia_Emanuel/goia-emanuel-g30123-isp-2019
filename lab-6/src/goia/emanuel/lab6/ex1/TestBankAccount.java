package goia.emanuel.lab6.ex1;

public class TestBankAccount {
    public static void main(String[] args) {
        BankAccount b1 = new BankAccount("Emanuel", 2000);
        BankAccount b2 = new BankAccount("Adrian", 1700);
        BankAccount b3 = b1;
        System.out.println(b1.equals(b3));
    }
}
