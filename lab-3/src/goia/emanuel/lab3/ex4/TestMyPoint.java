package goia.emanuel.lab3.ex4;

public class TestMyPoint {
    public static void main(String[] args) {
        MyPoint A = new MyPoint();
        MyPoint B = new MyPoint(2,2);
        A.setXY(4,5);
        B.setX(10);
        B.setY(8);
        System.out.println(A.distance(2,2));
        System.out.println(A.distance(B));
    }
}
