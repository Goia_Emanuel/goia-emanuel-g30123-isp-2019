package goia.emanuel.lab8.ex4;

public class CoolingUnit extends Unit {
    private boolean cooling;

    public CoolingUnit(boolean cooling){
        super(UnitType.ALARM.COOLING);
        this.cooling= cooling;
    }
    boolean isCooling(){
        return cooling;
    }
    public String toString(){
        return "CoolingUnit{" + "cooling="+cooling+"}";
    }
}
