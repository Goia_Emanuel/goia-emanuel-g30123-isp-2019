package goia.emanuel.lab8.ex4;

public class Unit {
    UnitType type;

    Unit(UnitType type){
        this.type = type;
    }

    UnitType getType(){
        return type;
    }
}
