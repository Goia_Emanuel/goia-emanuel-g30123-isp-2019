package goia.emanuel.lab4.ex4;


import goia.emanuel.lab4.ex2.Author;

import java.util.Arrays;

public class TestBook {
    public static void main(String[] args) {
        Author[] a = new Author[5];
        a[0] = new Author("Maria", "maria@gmail.com", 'f');
        a[1] = new Author("Ioana", "ioana@gmail.com", 'f');
        a[2] = new Author("Geanina", "geanina@gmail.com", 'f');
        a[3] = new Author("Costin", "costin@gmail.com", 'm');
        a[4] = new Author("Ion", "ion@gmail.com", 'm');
        Book b = new Book("Moartea Caprioarei", a, 59.99, 17);
        System.out.println(b.toString() + "\n");

        b.setPrice(29.99);
        b.setQtyInStock(40);
        System.out.println(Arrays.toString(b.getAuthors()) + " " + b.getName() + " " + b.getPrice() + " " + b.getQtyInStock());
    }
}
