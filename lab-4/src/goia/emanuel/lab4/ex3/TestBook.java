package goia.emanuel.lab4.ex3;

import goia.emanuel.lab4.ex2.Author;

public class TestBook {
    public static void main(String[] args) {
        Author author1 = new Author("Emanuel", "goia_emanuel@yahoo.com", 'm');
        Book book1 = new Book("Arta Razboiului", author1, 35.98, 5);
        String name = book1.getName();
        String author = String.valueOf(book1.getAuthor());
        double price = book1.getPrice();
        int qtyInStock = book1.getQtyInStock();
        System.out.println("Name: " + name);
        System.out.println("Author: " + author);
        System.out.println("Price: " + price);
        System.out.println("Quantity: " + qtyInStock);

        book1.setPrice(15.45);
        book1.setQtyInStock(35);
        System.out.println("Price: " + book1.getPrice() + " -> " + "Quantity: " + book1.getQtyInStock());

    }
}
