package goia.emanuel.lab4.ex6;

public class Test {
    public static void main(String[] args) {
        Shape s = new Shape("green",false);
        s.setColor("orange");
        s.setFilled(true);
        System.out.println(s.getColor() + " " + s.isFilled()+" " + s.toString());

        Circle c = new Circle(8,"blue",true);
        c.setRadius(5);
        System.out.println(c.getArea()+" "+c.getPerimeter()+" "+c.getRadius()+" "+c.toString());

        Rectangle r = new Rectangle(5.0,3.0,"red",true);
        r.setLength(3);
        r.setWidth(5);
        System.out.println(r.getArea()+" "+r.getLength()+" "+r.getPerimeter()+" "+r.getWidth()+" "+r.toString());

        Square s1 = new Square(5,"yellow",true);
        s1.setLength(2);
        System.out.println(s1.getSide());
        s1.setSide(4);
        System.out.println(s1.getSide());
        s1.setWidth(1);
        System.out.println(s1.getSide());
        System.out.println(s1.toString());
    }
}

