package goia.emanuel.lab4.ex6;

public class Circle extends Shape {
    private double radius;

    private Circle() {
        this.radius = 1.0;
    }

    private Circle(double radius) {
        this.radius = radius;
    }

    Circle(double radius, String color, boolean filled) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return 2 * 3.14 * this.radius * this.radius;
    }

    public double getPerimeter() {
        return 2 * 3.14 * this.radius;
    }

    @Override
    public String toString() {
        return "Cicrcle (radius:" + radius + ")" + "Subclass of" + super.toString();
    }
}
