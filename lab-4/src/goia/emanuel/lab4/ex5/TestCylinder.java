package goia.emanuel.lab4.ex5;

public class TestCylinder {
    public static void main(String[] args) {
        Cylinder c1 = new Cylinder(0);
        Cylinder c2 = new Cylinder(12);
        Cylinder c3 = new Cylinder(15, 7);
        System.out.println(c1.getHeight() + " " + c2.getHeight() + " " + c3.getHeight() + "\n");
        System.out.println(c1.getVolume() + " " + c2.getVolume() + " " + c3.getVolume());
    }
}
