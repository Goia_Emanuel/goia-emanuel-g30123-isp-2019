package goia.emanuel.lab12.ex4;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BankAccountTest {

    @Test
    public void increase() {
        BankAccount a = new BankAccount("a01", 100);
        a.increase(10);
        assertEquals(110, (long)a.getBalance());
    }

    @Test
    public void decrease() {
        BankAccount a = new BankAccount("a01", 100);
        a.decrease(10);
        assertEquals(90, a.getBalance());
    }
}
