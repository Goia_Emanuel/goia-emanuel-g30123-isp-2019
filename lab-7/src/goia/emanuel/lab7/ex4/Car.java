package goia.emanuel.lab7.ex4;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Car implements Serializable {
    private static ArrayList<Car> carList = new ArrayList<Car>();
    private String model;
    private double price;

    public Car() {
    }

    public Car(String model, double price) {
        this.model = model;
        this.price = price;
        carList.add(this);
    }

    public String getModel() {
        return model;
    }

    public double getPrice() {
        return price;
    }

    public static void serialize() {
        try {
            FileOutputStream fos = new FileOutputStream("temp.out", false);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(carList);
            oos.close();
        } catch (IOException e) {
            System.out.println("IO Error!");
        }
    }

    public static void deserialize() {
        try {
            FileInputStream fis = new FileInputStream("temp.out");
            ObjectInputStream oin = new ObjectInputStream(fis);
            carList = (ArrayList<Car>) oin.readObject();
            fis.close();
            for (int i = 0; i < carList.size(); i++) {
                System.out.println(carList.get(i).getModel() + " " + carList.get(i).getPrice());
            }
        } catch (IOException e) {
            System.out.println("IO Error!");
        } catch (ClassNotFoundException e) {
            System.out.println("Error! Class not found!");
        }
    }

    public void addCar() {
        Scanner input = new Scanner(System.in);
        System.out.println("Car name: ");
        model = input.nextLine();
        System.out.println("Car price: ");
        price = input.nextInt();
        Car newCar = new Car(model, price);
    }
}
