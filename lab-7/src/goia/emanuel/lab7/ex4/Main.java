package goia.emanuel.lab7.ex4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Car newCar = new Car();
        Scanner input = new Scanner(System.in);
        int opt = -1;
        while (opt != 4) {
            System.out.println("\n1) Add car\n2) Save car objects\n3) Read and display objects\n4) Exit\n");
            System.out.println("Alegeti optiunea: ");
            opt = input.nextInt();
            switch (opt) {
                case 1:
                    newCar.addCar();
                    break;
                case 2:
                    Car.serialize();
                    break;
                case 3:
                    Car.deserialize();
                    break;
                case 4:
                    System.exit(0);
                default:
                    System.out.println("Invlid Option!");
            }
        }

    }
}
