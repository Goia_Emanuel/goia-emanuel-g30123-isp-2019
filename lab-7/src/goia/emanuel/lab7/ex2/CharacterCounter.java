package goia.emanuel.lab7.ex2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CharacterCounter {
    public static void main(String[] args) {
        Scanner readChar = new Scanner(System.in);
        int index, nrAp = 0;
        System.out.print("Enter a character: ");
        char ch = readChar.nextLine().charAt(0);

        try {
            File file = new File("data.txt");
            Scanner readFromFile = new Scanner(file);
            while (readFromFile.hasNextLine()) {
                String line = readFromFile.nextLine();
                index = line.indexOf(ch);
                if (index != -1)
                    while (index >= 0) {
                        nrAp++;
                        index = line.indexOf(ch, index + 1);
                    }
            }
            System.out.print("Total number of aparitions of the character '" + ch + "' is: " + nrAp);
        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
        }

    }

}
