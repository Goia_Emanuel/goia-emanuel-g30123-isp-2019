package goia.emanuel.lab5.ex4;

import goia.emanuel.lab5.ex3.LightSensor;
import goia.emanuel.lab5.ex3.TemperatureSensor;

public class Controller {
    public TemperatureSensor tempSensor = new TemperatureSensor();
    public LightSensor lightSensor = new LightSensor();

    private static Controller c;

    private Controller() {
    }

    public static Controller getC() {
        if (c == null) {
            c = new Controller();
        }
        return c;
    }

    public void control() throws InterruptedException {
        int s = 0;
        while (s <= 20) {
            System.out.println("Tempeperature: " + tempSensor.readValue() + "\u00B0C");
            System.out.println("Light: " + lightSensor.readValue());
            Thread.sleep(1000);
            s++;
        }
    }
}
