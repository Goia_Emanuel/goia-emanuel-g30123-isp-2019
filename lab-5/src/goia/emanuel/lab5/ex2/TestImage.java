package goia.emanuel.lab5.ex2;

public class TestImage {
    public static void main(String[] args) {
       ProxyImage image = new ProxyImage("Real");
       image.display();
       image=new ProxyImage("Rotated");
       image.display();
    }
}
