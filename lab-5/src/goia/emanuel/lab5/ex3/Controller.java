package goia.emanuel.lab5.ex3;

public class Controller {
    public TemperatureSensor tempSensor = new TemperatureSensor();
    public LightSensor lightSensor = new LightSensor();

    public void control() throws InterruptedException {
        int a = 0;
        while (a <= 20) {
            System.out.println("Tempeperature: " + tempSensor.readValue() + "\u00B0C");
            System.out.println("Light: " + lightSensor.readValue());
            Thread.sleep(1000);
            a++;
        }
    }
}
